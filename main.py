from multiprocessing import Manager
from multiprocessing import Process
from ctypes import c_char_p
from utils import flight
from utils import pid
import argparse

ap = argparse.ArgumentParser()
ap.add_argument("-s", "--simulation", default=False,
	help="Kodun simulasyonda mi gercekte mi calisagini belirler.")
args = vars(ap.parse_args())

if args["simulation"]:
	from utils import cv_sim
	from utils import img_converter
else:
	from utils import cv 

with Manager() as manager:
	# PID <-----< CV
	up_left_x = manager.Value("i", 0)
	up_left_y = manager.Value("i", 0)
	bottom_right_x = manager.Value("i", 0)
	bottom_right_y = manager.Value("i", 0)

	# AUTOPILOT <-----< PID
	pitch = manager.Value("f", 0.0)
	yaw = manager.Value("f", 0.0)
	throttle = manager.Value("f", 0.0)

	# CV >-------> AUTOPILOT, PID
	detected = manager.Value("i", 0)

	processPID = Process(target=pid,
		args=(up_left_x, up_left_y, bottom_right_x, bottom_right_y, detected,
			pitch, yaw, throttle))

	processAUTOPILOT = Process(target=flight,
		args=(detected, pitch, yaw, throttle))
	if args["simulation"]:
		sim_frame = manager.list()
		processCV = Process(target=cv_sim,
			args=(up_left_x, up_left_y, bottom_right_x, bottom_right_y, detected, sim_frame))
		processIMG = Process(target=img_converter, args=(sim_frame, ))

	else:
		processCV = Process(target=cv,
			args=(up_left_x, up_left_y, bottom_right_x, bottom_right_y, detected))


	processAUTOPILOT.start()
	processCV.start()
	processPID.start()
	if args["simulation"]:
		processIMG.start()
		processIMG.join()

	processAUTOPILOT.join()
	processCV.join()
	processPID.join()
