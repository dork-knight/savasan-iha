Gereksinimler
-ROS (Simulasyon için) 
-Gazebo (Simulasyon için)
-Opencv 3.4 veya daha üst versiyon (YOLO'nun CUDA destekli çalışması için 4.2 veya daha ust versiyon)
-Dronekit
-Ardupilot (2 tane)

Dosyalar

-gazebo_modeller_ve_dunya : Gazebo modellerini ve dunyasını içerir. Bu dosyaları gazebo path inize uygun bir yere ekleyin.

-Tiny-YOLO: YOLO weightlerini ve cfg dosyasını barındıran dosyadır.

-utils:
	-core.py: Ana fonksiyonların bulunduğu dosyadır.
	-pid.py: PID classının bulunduğu dosyadır.
	-detector.py: YOLO classının bulunduğu dosyadır.
	-utils.py: Yardımcı fonksiyonların olduğu dosyadır

-fly.py: Rakip dronu klavye tuşlarıyla kontrol etmeye yarayan dosyadır.

-Server.py: Yarışma ortamındaki server ı taklit eden dosyadır. Rakip dronun bilgilerini 0.3-1.2 saniye aralıklarla otonom uçan drona iletir.

-main.py: Şavaşan iha kodunu çalıştıracağımız dosyadır.

Yapılması Gerekenler:

-Kontrol etmemiz gereken 2 drone olduğundan 2 farklı ardupilota ihtiyacımız var. Ardupilot dosyanızı kopyalarak ardupilot2 adında başka bir dosya oluşturunuz.
-roscore komutu ile ros serverını başlatın.
-rosrun gazebo_ros gazebo /usr/share/gazebo-9/worlds/savasan-iha-deneme.world komutu ile gazeboyu başlatın. 
-ardupilot dosyasına gelin. " Tools/autotest/sim_vehicle.py -f gazebo-iris --vehicle ArduCopter --map -I 0 " komutu ile otonom dronu başlatın.
-ardupilot2 dosyasına gelin. " Tools/autotest/sim_vehicle.py -f gazebo-iris --vehicle ArduCopter --map -I 1 " komutu ile rakip dronu başlatın.
-fly.py ile rakip dronu kaldırıp klavye tuşlarıyla kontrol edebilirsiniz.
-Server.py ı başlatın. Bu kod iki drona da bağlanacak ve aralarındaki veri akışını sağlayacaktır.
-" python main.py --simulation True " komutu ile simulayon kodunu başlatabilirsiniz 
 veya	" python main.py --simulation False " komutu ile gerçek hayat kodunu başlatabilirsiniz.

Not: Similasyon sırasında pid tuning ini daha kolay yapabilmek için görüntü işlemeyi manual olarak kapatıp açmaya 
ihtiyaç duyduk. Bu yüzden "q" tuşunu açma-kapama tuşu olarak atadık. Simulasyon başladığında açılan ekranda "q" tuşuna basarak görüntü işlemeyi başlatabilir ve isterseniz similasyon sırasında yine q tuşuna basarak durdurabilirsiniz. 
