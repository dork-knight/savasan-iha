from dronekit import LocationGlobalRelative
from dronekit import LocationGlobal
from dronekit import VehicleMode
from dronekit import APIException
from dronekit import connect
from pymavlink import mavutil
from geopy import distance
import numpy as np
import time
import socket
import math
import argparse
import cv2

def grab_coords(result, res_x, res_y) :
	if (result == []) :
		return ()
	rects = []
	dist = 0
	closest = 100000
	half_x = res_x // 2
	half_y = res_y // 2
	const1 = float(res_x)/416.0
	const2 = float(res_y)/416.0
	area = res_x * res_y
	for i in range(len(result)) :
		(obj, prob ,(x , y, width, height)) = result[i]
		dist = (((half_x-x)**2)+((half_y-y)**2))**0.5
		x = int(x - (width / 2.0))
		y = int(y - (height / 2.0))
		if (dist<closest):
			closest = dist
			rects = [obj ,prob, int(x*const2), int(y*const1), int(width*const2), int(height*const1)]
	return rects

def put_rect(image) :
	h, w = image.shape[:2]
	x = int(w / 4.0)
	y = int(h / 10.0)
	rW = int(w / 2.0)
	rH = int((h * 4.0) / 5.0)	
	cv2.rectangle(image, (x,y), (x+rW, y+rH), (240,127,37), 3)
	return (x, y, x+rW, y+rH)


def ihamibagla():

	#iha = connect("/dev/serial/by-id/usb-ArduPilot_Pixhawk1_330037000E51353239333634-if00",wait_ready=True)
	iha = connect("127.0.0.1:14550",wait_ready=True,timeout=100)
	return iha



def armolveuc(hedefirtifa, iha):
    while iha.is_armable==False:
        print("Arm icin gerekli sartlar saglanamadi. Lutfen bekleyin")
        time.sleep(1.5)
    print("Ihamiz su anda arm edilebilir")

    iha.mode=VehicleMode("GUIDED")
    while iha.mode!='GUIDED':
        print("GUIDED moduna gecis icin bekleniyor")
        time.sleep(1.5)
    print("GUIDED moduna gecis yapildi")
    iha.armed=True
    while iha.armed==False:
        print("Arm icin bekleniliyor")
    print("Ihamiz arm olmustur kolay gelsin")

    iha.simple_takeoff(hedefirtifa)
    #iha.airspeed = 1000
    #iha.parameters['WPNAV_SPEED'] = 1000    

    while iha.location.global_relative_frame.alt<=0.94*hedefirtifa:
        print("Su anki yukseklik: %d"%iha.location.global_relative_frame.alt)
        time.sleep(0.5)
    print("Hedef yukseklige ulasildi")

def Yaklasim_Alg(hedefkonum,iha): 
	print("Yaklasim algoritmasi")
	if iha.mode.name != 'GUIDED' :
		iha.mode=VehicleMode("GUIDED")
	print(iha.mode.name)
	iha.simple_goto(hedefkonum)
       	iha.parameters['WPNAV_SPEED'] = 2000
	iha.parameters['WPNAV_ACCEL'] = 500 
	print(iha.airspeed)
	#time.sleep(0.75)

def wp_guncelle(Enlem,Boylam,Irtifa):
	wp=LocationGlobal(Enlem/1e7,Boylam/1e7,Irtifa/1e3)
	#print("Waypointe gidiliyor")
	return wp


def yaw(heading, iha):
	msg = iha.message_factory.command_long_encode(
		0, 0,    
		mavutil.mavlink.MAV_CMD_CONDITION_YAW, 
		0, 
		heading,    
		0,          
		1 if (heading>0 or heading<=180) else -1,          
		0, 
		0, 0, 0)    
    
	iha.send_mavlink(msg)

def send_ned_velocity(velocity_x, yaw_rate, velocity_z, iha):

	msg = iha.message_factory.set_position_target_local_ned_encode(
          0,
          0, 0,
          mavutil.mavlink.MAV_FRAME_BODY_OFFSET_NED, 
          0b0000011111000111,
          0, 0, 0, 
          velocity_x, 0, velocity_z,
          0, 0, 0,
          0, math.radians(yaw_rate))

        iha.send_mavlink(msg)


def heading(enl1,boy1,enl2,boy2):
	boylfarki = boy2 - boy1
	a = math.sin(boylfarki) * math.cos(enl2)
	b = math.cos(enl1)*math.sin(enl2) - math.sin(enl1)*math.cos(enl2)*math.cos(boylfarki);
	hdng = np.rad2deg(math.atan2(a, b))
	if hdng < 0: 
		hdng+= 360
	return hdng


def alt_farki(alt1,alt2):
	return abs(alt2-alt1)

def gstreamer_pipeline(capture_width=640, capture_height=480, display_width=640, display_height=480, framerate=9, flip_method=0) :   
	return ('nvarguscamerasrc ! ' 
	'video/x-raw(memory:NVMM), '
	'width=(int)%d, height=(int)%d, '
	'format=(string)NV12, framerate=(fraction)%d/1 ! '
	'nvvidconv flip-method=%d ! '
	'video/x-raw, width=(int)%d, height=(int)%d, format=(string)BGRx ! '
	'videoconvert ! '
	'video/x-raw, format=(string)BGR ! appsink'  % (capture_width,capture_height,framerate,flip_method,display_width,display_height))

enlem = 0
boylam= 0
irtifa = 0
def rakip_iha(iha):
	@iha.on_message("RAKIP_IHA_KONUM")
	def listener(self, name, msg):
		global enlem
		global boylam
		global irtifa
		enlem = msg.enlem
		boylam = msg.boylam
		irtifa = msg.irtifa
	return (enlem, boylam, irtifa)

def uzaklik(enlem, boylam, irtifa ,iha):
	bizim_konum=(iha.location.global_relative_frame.lat,iha.location.global_relative_frame.lon) 
	rakip_konum=(enlem,boylam)
	konfarki=distance.distance(bizim_konum, rakip_konum).meters
	irtfarki=abs(irtifa-iha.location.global_relative_frame.alt)
	net_uzaklik=math.sqrt(konfarki*konfarki+irtfarki*irtfarki)
	#print(konfarki, irtfarki, net_uzaklik)
	return irtfarki, konfarki, net_uzaklik

def proper_detection(rects, confidences):
	return rects[0]
