from dronekit import LocationGlobalRelative
from dronekit import VehicleMode
from utils import gstreamer_pipeline
from utils import send_ned_velocity
from utils import proper_detection
from detector import YOLO_Detector
from utils import ihamibagla
from utils import rakip_iha
from utils import uzaklik
from pid import PID
from utils import heading
from utils import yaw as _yaw
import time
import cv2
import signal
import sys
import math
import imutils


def signal_handler(sig, frame):
	print("[INFO] Exiting...")
	sys.exit()

def flight(detected, pitch, yaw, throttle):
	signal.signal(signal.SIGINT, signal_handler)
	prev_time = time.time()
	prev1_time = time.time()
	iha = ihamibagla()
	while not iha.armed :
		print("[INFO] Yer istasyonundan arm komutu bekleniyor ...")
	time.sleep(2.5)
	iha.mode = VehicleMode("GUIDED")
	iha.simple_takeoff(10)

	print("[INFO] iha GUIDED moda alindi ...")
	iha.parameters["WPNAV_ACCEL"] = 250
	while (iha.location.global_relative_frame.alt <= 0.94 * 3) :
		print("[INFO] Su anki yukseklik: {} ...".format(iha.location.global_relative_frame.alt))
		time.sleep(0.5)

	iha.parameters['WP_YAW_BEHAVIOR']=4
	while True:
		if iha.mode == 'GUIDED':
			(enlem, boylam, irtifa) = rakip_iha(iha)
			dikey_uzaklik, yatay_uzaklik, net_farki = uzaklik(enlem, boylam, irtifa ,iha)
			if time.time() - prev1_time > 0.5 :
				prev1_time = time.time()
				print("dikey uzaklik", dikey_uzaklik)
				#print("altitude", irtifa - iha.location.global_relative_frame.alt)
			if detected.value == 0 and irtifa>5 and not net_farki<4:
				if time.time() - prev_time > 1.5 :
					prev_time = time.time()
					hdg = heading(iha.location.global_relative_frame.lat,iha.location.global_relative_frame.lon,enlem,boylam)
					#_yaw(hdg, iha)
					#print("Hedefe gidiliyor (Waypoint)")
					#time.sleep(0.75)

					#iha.simple_goto(LocationGlobalRelative(enlem, boylam, irtifa))


			elif detected.value:
				#print("Hedef tespit edildi !")
				#send_ned_velocity(pitch.value, yaw.value, throttle.value, iha)
				send_ned_velocity(0, 0, -throttle.value, iha)
			#print(detected.value)
			if net_farki < 2 :
				#print("Hedef cok yakin !!!!")
				send_ned_velocity(-3, 0, 0, iha)




def cv(x1, y1, x2, y2, detected):
	signal.signal(signal.SIGINT, signal_handler)
	out = cv2.VideoWriter("output.avi",
		cv2.VideoWriter_fourcc("M","J","P","G"), 30, (640, 480))
	weights = "Tiny-YOLO/yolo-drone_final.weights"
	cfg = "Tiny-YOLO/yolo-drone.cfg"
	detector = YOLO_Detector(weights, cfg)
	#cam = cv2.VideoCapture(gstreamer_pipeline(), cv2.CAP_GSTREAMER)
	cam = cv2.VideoCapture(0)
	while True:
		grabbed, frame = cam.read()
		rects, confidences, classIDs = detector.detect(frame)

		if len(rects) == 0:
			detected.value = 0
		else:
			detected.value = 1
			detected_uav = proper_detection(rects, confidences)
			x1.value = detected_uav[0]
			y1.value = detected_uav[1]
			x2.value = detected_uav[2]
			y2.value = detected_uav[3]
			print("Detected at :", detected_uav)
		out.write(frame)
		cv2.imshow("detection", frame)
		cv2.waitKey(1)
	

def cv_sim(x1, y1, x2, y2, detected, sim_frame):
	signal.signal(signal.SIGINT, signal_handler)
	out = cv2.VideoWriter("output.avi".format(time.time()),
		cv2.VideoWriter_fourcc("M","J","P","G"), 30, (640, 480))
	weights = "Tiny-YOLO/yolo-drone_final.weights"
	cfg = "Tiny-YOLO/yolo-drone.cfg"
	detector = YOLO_Detector(weights, cfg)
	time.sleep(3)
	init_detection = False
	while True:
		frame = sim_frame[-1]
		if init_detection:
			#rects, confidences, classIDs = detector.detect(frame)
			frame1 = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
			(T, threshInv) = cv2.threshold(frame1, 150, 255, cv2.THRESH_BINARY_INV)
			edged = cv2.bitwise_and(frame1, frame1, mask = threshInv)
			cnts = cv2.findContours(edged.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
			cnts = imutils.grab_contours(cnts)
			cnts = sorted(cnts, key=cv2.contourArea, reverse=True)
			if len(cnts) == 0:		
				detected.value = 0
				#print("NO detetection")

			else:
				x, y, w, h = cv2.boundingRect(cnts[0])
				detected.value = 1
				#detected_uav = proper_detection(rects, confidences)
				x1.value = x#detected_uav[0]
				y1.value = y#detected_uav[1]
				x2.value = x+w#detected_uav[2]
				y2.value = y+h#detected_uav[3]
				#print("Detected at :", (x1.value, y1.value), (x2.value, y2.value))
				cv2.rectangle(frame, (x1.value, y1.value), (x2.value, y2.value), (0,255,0), 1)
		else:
			detected.value = 0	
		out.write(frame)
		cv2.imshow("detection", frame)
		k = cv2.waitKey(1)
		if k == ord("q"):
			init_detection = not init_detection


def pid(x1, y1, x2, y2, detected, pitch, yaw, throttle):
	signal.signal(signal.SIGINT, signal_handler)
	_pitch = (0.6, 0.003, 0.35)
	_yaw = (2.6, 0.1, 0.075)
	#_throttle = (1, 0.01, 0.1)
	_throttle = (1.75, 0.001, 1)
	_pid = PID(_pitch, _yaw, _throttle)
	lost_info = [False]
	while True:
		if detected.value == 0:
			_pid = PID(_pitch, _yaw, _throttle)
			pitch.value = 0
			yaw.value = 0
			throttle.value = 0
		else :
			pitch.value, yaw.value, throttle.value = _pid.update(x1.value,
				y1.value, x2.value, y2.value)
			#print(detected.value, pitch.value, yaw.value, throttle.value)
			time.sleep(0.015)


def img_converter(f):
	signal.signal(signal.SIGINT, signal_handler)

	from cv_bridge import CvBridge, CvBridgeError
	from sensor_msgs.msg import Image
	from std_msgs.msg import String
	import rospy

	global i
	i = 0
	def callback(data):
		global i
		try:
			cv_image = bridge.imgmsg_to_cv2(data, "bgr8")
		except CvBridgeError as e:
			print(e)
		
		f.append(cv_image)
		if i > 3:
			del f[0]
		i = i + 1
		#print(f)
		cv2.imshow("zxc", cv_image)
		cv2.waitKey(1)

		try:
			image_pub.publish(bridge.cv2_to_imgmsg(cv_image, "bgr8"))
		except CvBridgeError as e:
			print(e)
	image_pub = rospy.Publisher("image_topic_2",Image)
	bridge = CvBridge()
	image_sub = rospy.Subscriber("/drone/camera1/image_raw",Image,callback)

	rospy.init_node('image_converter', anonymous=True)
	try:
		rospy.spin()
	except KeyboardInterrupt:
		print("Shutting down")
