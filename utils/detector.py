import numpy as np
import time
import cv2
import os

class YOLO_Detector :

	def __init__(self, weights, cfg, confidence=0.1):
		# Classlarin isimlerini tanimla
		self.Labels = ["drone"]

		# YOLO'yu yukle
		print("[INFO] YOLO diskten yukleniyor ...")
		self.net = cv2.dnn.readNetFromDarknet(cfg, weights)

		# Eger opencv 4.2 ise veya daha guncel bir versiyonsa
		# CUDA destegi ile GPU da, degilse CPU da baslat
		if (int(cv2.__version__[0]) >= 4) and (int(cv2.__version__[0]) >= 2):
			print("[INFO] YOLO CUDA backendiyle baslatiliyor ...")
			self.net.setPreferableBackend(cv2.dnn.DNN_BACKEND_CUDA)
			self.net.setPreferableTarget(cv2.dnn.DNN_TARGET_CUDA)
		else :
			print("[INFO] YOLO CPU'da baslatiliyor ...")

		# Output'larin olacagi YOLO katmanlarinin isimlerini al
		ln = self.net.getLayerNames()
		self.ln = [ln[i[0] - 1] for i in self.net.getUnconnectedOutLayers()]
	
		# Detection icin confidence thresholdunu belirle
		self.conf = confidence

	def detect(self, frame):
		(H, W) = frame.shape[:2]
		
		# Input resmi detection icin (416, 416) olacak sekilde boyutlandir
		# RGB'ye donustur ve butun piksel degerlerini 0-1 arasina sikistir  
		blob = cv2.dnn.blobFromImage(frame, 1 / 255.0, (416, 416),
			swapRB=True, crop=False)

		# Resmi input olarak ver ve YOLO katmanlarindaki outputlari al
		self.net.setInput(blob)
		start = time.time()
		layerOutputs = self.net.forward(self.ln)
		end = time.time()

		#print("[INFO] FPS {:.2f}".format(1/(end - start)))

		# Detection lardan confidence si thresholdun uzerinde olanlari al
		boxes = []
		confidences = []
		classIDs = []
		for output in layerOutputs:
			for detection in output:
				scores = detection[5:]
				classID = np.argmax(scores)
				confidence = scores[classID]
				if confidence >= self.conf:
					box = detection[0:4] * np.array([W, H, W, H])
					(centerX, centerY, width, height) = box.astype("int")

					x1 = int(centerX - (width / 2))
					y1 = int(centerY - (height / 2))

					x2 = int(centerX + (width / 2))
					y2 = int(centerY + (height / 2))

					boxes.append([x1, y1, x2, y2])
					confidences.append(float(confidence))
					classIDs.append(classID)

		# Confidence elemesinden gecen detection lara Non-Max Supression uygula  
		idxs = cv2.dnn.NMSBoxes(boxes, confidences, self.conf, 0.1)

		# NMS den geriye kalan detectionlari final output olarak don
		final_boxes = []
		final_confidences = []
		final_classIDs = []

		if len(idxs) > 0:
			for i in idxs.flatten():
				final_boxes.append(boxes[i])
				final_confidences.append(confidences[i])
				final_classIDs.append(classIDs[i])

		return final_boxes, final_confidences, final_classIDs
