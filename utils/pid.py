import math
import time
# PID sinifini tanimla
class PID :

	def __init__(self, pitch, yaw,
			throttle, coz_x=640, coz_y=480,
				mid_throttle=0, mid_pitch=0, mid_yaw=0,cam_fov_x=62,cam_fov_y=46.5):
		
		self.half_cam_fov_x=cam_fov_x/2.0
		self.half_cam_fov_y=cam_fov_y/2.0
		# Pitch sabitlerini tanimla
		self.kpp = pitch[0]
		self.kip = pitch[1]
		self.kdp = pitch[2]

		
		# Throttle sabitlerini tanimla
		self.kpy = yaw[0]
		self.kiy = yaw[1]
		self.kdy = yaw[2]

		# Yaw-rate sabitleri
		self.kpt = throttle[0]
		self.kit = throttle[1]
		self.kdt = throttle[2]


		# Cozunurlugu ve hedef alani tanimla
		self.cozunurluk_x = coz_x
		self.cozunurluk_y = coz_y
		
                self.hedef_uzunluk = 5
                

		# Onceki errorlari tanimla
		self.past_error_pitch = 0
		self.past_error_throttle = 0
		self.past_error_yaw = 0
	

		# Errorlari tanimla
		self.error_ipitch = 0
		self.error_ithrottle = 0
		self.error_iyaw = 0
		self.p_time = time.time()

	def update(self, sol_ustx, sol_usty, sag_altx, sag_alty):

		# Dikdortgenlerin diger koselerini tanimla
		sol_altx = sol_ustx
		sol_alty = sag_alty
		sag_ustx = sag_altx
		sag_usty = sol_usty

		# Sanal pikselsel uzakliklari hesapla
		half_of_pixels_on_x=self.cozunurluk_x/2
		half_of_pixels_on_y=self.cozunurluk_y/2
		virtual_depth_x=(half_of_pixels_on_x/math.tan(self.half_cam_fov_x))
		virtual_depth_y=(half_of_pixels_on_y/math.tan(self.half_cam_fov_y))

		# Kilitlenme dikdortgeninin merkez koordinatlarini
		# ve alanini hesapla
		ort_x = (sol_ustx+sag_altx)/2.0
		ort_y = (sol_usty+sag_alty)/2.0 
		

		#pitch icin drone uzakligi hesapla
                uzunluk_x = sag_ustx-sol_ustx
		uzaklik_x = (virtual_depth_x*0.47)/(float(uzunluk_x) + 0.0005)	

                uzunluk_y = sag_alty-sol_usty
		uzaklik_y = (virtual_depth_y*0.23)/(float(uzunluk_y) + 0.0005)

		uzaklik_ort = (uzaklik_x+uzaklik_y)/2	

		#yaw icin derece cinsinden error hesapla
		pixel_x_error=ort_x-half_of_pixels_on_x
		error_yaw=math.atan(pixel_x_error/virtual_depth_x)
		error_yaw = (error_yaw*180)/math.pi


		#throttle icin rakip yukseklik hesapla
		pixel_y_error=half_of_pixels_on_y-ort_y
		height_error=(pixel_y_error*(-uzaklik_ort))/virtual_depth_y


		# Yaw icin PID hesapla
		
		if (error_yaw + self.error_iyaw)*self.kiy < 30 and (error_yaw + self.error_iyaw)*self.kiy > -30:
			self.error_iyaw = error_yaw + self.past_error_yaw

		errord_yaw = error_yaw - self.past_error_yaw

		pid_yaw = self.kpy*error_yaw + self.kiy*self.error_iyaw + self.kdy*errord_yaw
		
		



		# throttle icin PID hesapla
		
		if (height_error + self.error_ithrottle)*self.kit < 5 and (height_error + self.error_ithrottle)*self.kit > -2.5 : 
			self.error_ithrottle = height_error + self.error_ithrottle

		errord_throttle = height_error - self.past_error_throttle

		pid_throttle = self.kpt*height_error + self.kit*self.error_ithrottle + self.kdt*errord_throttle
			
	
		


                # Pitch icin PID hesapla
		
                error_pitch = -(uzaklik_ort+5)  #5 metre uzakta olsun istiyoruz
		self.error_ipitch+=error_pitch
		integral_pitch_velocity=self.kip*self.error_ipitch
		
		if integral_pitch_velocity>=5 :
			integral_pitch_velocity=5
		elif integral_pitch_velocity<=-5 :
			integral_pitch_velocity=-5
			
		errord_pitch = error_pitch - self.past_error_pitch            
                pid_pitch= self.kpp*error_pitch + self.kdp*errord_pitch + integral_pitch_velocity
	

		# Onceki error degerini guncelle
		self.past_error_pitch = error_pitch
		self.past_error_throttle = height_error
		self.past_error_yaw = error_yaw
		#Son hiz ve rate degerlerini tanimla
		throttle=pid_throttle
		pitch=pid_pitch
		yaw_rate=pid_yaw
		#Sinirlari belirle
                if throttle > 10:
                    throttle = 10
                elif throttle < -5 :
                    throttle = -5
                else :
                    throttle = throttle

                if yaw_rate > 60:
                    yaw_rate = 60
                elif yaw_rate < -60:
                    yaw_rate = -60
                else :
                    yaw_rate = yaw_rate

                if pid_pitch > 10:
                    pid_pitch = 10
                elif pid_pitch < -10:
                    pid_pitch = -10
                else:
		    pitch = pitch

		if (time.time() - self.p_time > 0.5):
			self.p_time = time.time()
			print("pitch", height_error)
			#print("a",error_pitch, height_error, error_yaw, -yaw_rate)
			#print("uzaklik", uzaklik_x, uzaklik_y, uzaklik_ort)

		return (pitch ,-yaw_rate, throttle)

# Birim testi
if __name__ == "__main__" :
	_pitch = (0.34, 0.1, 0.1)
	_yaw = (0.48, 0.08, 0.075)
	_throttle = (1.4, 0.1, 0.1)
	_pid = PID(_pitch, _yaw, _throttle)
	result = _pid.update(50, 100, 70, 200)
	print("Final_Throttle : {}, Final_Yaw : {}, Final_Pitch : {}".format(result[0],
		result[1], result[2]))
	
